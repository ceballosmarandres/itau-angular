import { RouterModule, Routes } from '@angular/router';
import { ViewListComponent } from './components/view-list/view-list.component';
import { CreateItemComponent } from './components/create-item/create-item.component';
import { ViewTodosComponent } from './components/view-todos/view-todos.component';


const APP_ROUTES: Routes = [
    { path: 'view-list', component: ViewListComponent },
    { path: 'view-todos', component: ViewTodosComponent },
    { path: 'create-item/:id', component: CreateItemComponent },
    { path: '**', pathMatch: 'full', redirectTo: 'view-todos'}
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES)