import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ListTodo } from '../models/list-todo.model';

@Injectable({
  providedIn: 'root'
})
export class ListService {

  private url = "https://itau-4dd3b.firebaseio.com"
  constructor( private http: HttpClient) { }

  getListTodo() {
    return this.http.get('https://jsonplaceholder.typicode.com/todos').toPromise();   
  }

  getListFirebase( id:string ) {  
    return this.http.get(`${ this.url }/lists/${ id }.json`)
  }

  deleteList( id: string ) {
    return this.http.delete(`${ this.url }/lists/${ id }.json`)
  }

  getListsFirebase() {
    return this.http.get(`${ this.url }/lists.json`)
      .pipe(
        map( this.crearListArreglo )
      )
  }

  private crearListArreglo ( listsObj: object){
    const lists: ListTodo[] = [];
    if( listsObj === null ) {
      return [];
    }
    Object.keys( listsObj ).forEach( key => {
      const list: ListTodo = listsObj[key];
      list.id = key;

      lists.push( list)
    })

    return lists;
  }

  createList( list: ListTodo ) {
    return this.http.post(`${ this.url }/lists.json`, list)
      .pipe(
        map( (response : any ) => {
          list.id = response.name;
          return list;
        })
      )
  }

  updateList( list: ListTodo ){
    const listTemp = {
      ...list
  }

    delete listTemp.id;

    return this.http.put(`${ this.url }/lists/${ list.id }.json`, listTemp);
  }
}
