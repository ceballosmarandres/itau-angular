import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any, arg1: any, arg2:any): any {
    if(arg1 === ''){
      return value;
    }
    const resultList = [];
    for( const list of value ){
      if(arg2.filtro === 'id'){
        if(parseInt(arg1)){
          if( parseInt(arg1) === list.id ){
            resultList.push(list);
          }
        } else {
          if( list.id.toLowerCase().indexOf(arg1.toLowerCase()) > -1 ){
            resultList.push(list);
          }
        }
      } else {
        if( list.title.toLowerCase().indexOf(arg1.toLowerCase()) > -1 ){
          resultList.push(list);
        }
      }
    }
    return resultList;
  }

}
