import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

//Routes
import { APP_ROUTING } from './app.routes';

//Servicios
import { ListService } from './services/list.service';

//Components
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ViewListComponent } from './components/view-list/view-list.component';
import { CreateItemComponent } from './components/create-item/create-item.component';
import { FilterPipe } from './pipes/filter.pipe';
import { ViewTodosComponent } from './components/view-todos/view-todos.component';


@NgModule({
  declarations: [
    AppComponent,
    ViewListComponent,
    CreateItemComponent,
    FilterPipe,
    ViewTodosComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    APP_ROUTING
  ],
  providers: [ ListService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
