import { Component, OnInit } from '@angular/core';
import { NgForm }   from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ListTodo } from 'src/app/models/list-todo.model';
import { ListService } from 'src/app/services/list.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-create-item',
  templateUrl: './create-item.component.html',
  styleUrls: ['./create-item.component.css']
})
export class CreateItemComponent implements OnInit {

  list: ListTodo = new ListTodo();

  constructor( private _listsService: ListService, private route: ActivatedRoute ) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');

    if ( id !== 'add' ) {
      this._listsService.getListFirebase( id )
        .subscribe( (response : ListTodo) => {
          this.list = response;
          this.list.id = id;
        });
    }
  }

  guardar( form:NgForm ) {
    if(form.invalid){
      return;
    }

    Swal.fire({
      title:'Espere',
      text:'Guardando titulo',
      type:'info',
      allowOutsideClick: false
    });

    Swal.showLoading();

    let peticion: Observable<any>;

    if ( this.list.id ) {
      peticion = this._listsService.updateList( this.list);
    } else {
      peticion = this._listsService.createList( this.list);
    }

    peticion.subscribe( resp => {
      Swal.fire({
        text:'Se guardo correctamente',
        type:'success',
      });
    })
  }

}
