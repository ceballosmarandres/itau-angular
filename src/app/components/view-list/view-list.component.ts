import { Component, OnInit } from '@angular/core';
import { ListService } from 'src/app/services/list.service';
import { ListTodo } from 'src/app/models/list-todo.model';

@Component({
  selector: 'app-view-list',
  templateUrl: './view-list.component.html',
  styleUrls: ['./view-list.component.css'],
  providers: [ ListService ]
})

export class ViewListComponent implements OnInit {
  lists: ListTodo;
  filtros: any = ['id', 'titulo']
  filterList = '';
  loading = false;

  constructor( private _listsService: ListService ) { }
  filtroObj: any = {
    filtro:"titulo"
  }
  ngOnInit() {
    this.loading = true;
    this._listsService.getListTodo()
    .then((results: any) => {
      this.lists = results;
      this.loading = false; 
    }).catch(err => console.log('error', err))
  }

}
