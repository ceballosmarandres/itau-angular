import { Component, OnInit } from '@angular/core';
import { ListService } from 'src/app/services/list.service';
import { ListTodo } from 'src/app/models/list-todo.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-view-todos',
  templateUrl: './view-todos.component.html',
  styleUrls: ['./view-todos.component.css']
})
export class ViewTodosComponent implements OnInit {
  lists: ListTodo[] = [];
  filtros: any = ['id', 'titulo']
  filterList = '';
  loading = false;
  
  constructor( private _listsService: ListService ) { }
  filtroObj: any = {
    filtro:"titulo"
  }
  ngOnInit() {
    this.loading = true;
    this._listsService.getListsFirebase()
      .subscribe( response => {
        this.lists = response;
        this.loading = false; 
      })
  }


  deleteList( list:ListTodo, i: number ){
    Swal.fire({
      title:'¿Seguro?',
      text:`Esta seguro que desea eliminar el titulo ${list.title}`,
      showConfirmButton: true,
      showCancelButton: true
    }).then( response => {
      if( response.value){
        this.lists.splice(i, 1);
        this._listsService.deleteList(list.id).subscribe()
      }
    })
  }
}
